const express = require("express");
const app = express();
const routes = require("./routes/email");
const PORT = 5000;
const cors = require('cors');

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());

app.use("/sub", routes);

app.listen(PORT, () => {
  console.log(`Subscription Service is running at http://localhost:${PORT}`);
});
