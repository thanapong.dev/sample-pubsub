const { publishMessage } = require("../helper/pub-sub-config");
const topicName = "my-topic";

const welcome = (req, res) => {
  return res.status(200).json({
    success: true,
    message: "Welcome to User Profile Service:)",
  });
};

const createUser = async (req, res) => {
  let userObj = req.body;
  let messageId = await publishMessage(topicName, userObj);
  return res.status(200).json({
    success: true,
    message: `Message ${messageId} published :)`,
  });
};
module.exports = { welcome, createUser };
