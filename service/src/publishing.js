const express = require("express");
const app = express();
const routes = require("./routes/user");
const PORT = 3000;
const cors = require('cors');

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());

app.use("/pub", routes);

app.listen(PORT, () => {
  console.log(`Publishing service is running at http://localhost:${PORT}`);
});
